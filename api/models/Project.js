/**
 * Created by peter on 21/05/14.
 */
var Project = {
    adapter: 'mongo',
    attributes: {
        code: {
            type: 'STRING',
            required: true,
            maxLength: 3
        },
        type: {
            type: 'STRING',
            required: true
        },
        title: {
            type: 'STRING',
            required: true
        },
        status: 'INTEGER',
        getStatus: function() {
            if (this.status == 1) { return 'In Progress'}
            else { return 'Paused' }
        }
    }
};

module.exports = Project;