var Task = {
    adapter: 'mongo',
    attributes: {
        project : {
            type: 'STRING',
            required: true
        },
        progress: {
            type: 'INTEGER',
            maxLength: 2,
            required: true
        },
        status: {
            type: 'INTEGER'
        },
        title: {
            type: 'STRING',
            required: true
        },
        note: {
            type: 'STRING'
        }
    }
}

module.exports = Task;