var IndexController = {
    index: function(req, res) {
        Project.find(function(err, projects) {
            projects.forEach(function(project) {
                var tasksArray = {};
                Task.find(function(err, tasks) {
                    tasks.forEach(function(task) {
                        var taskToArray = task;
                        tasksArray.push(taskToArray);
                        console.log(tasksArray);
                    });
                });
                console.log(tasksArray);
                project['tasks'] = tasksArray;
            });

            res.view('home/index', {
                projects: projects
            });
        });
    },
    virgin: function(req, res) {
        res.view('home/virgin');
    }
}

module.exports = IndexController;
